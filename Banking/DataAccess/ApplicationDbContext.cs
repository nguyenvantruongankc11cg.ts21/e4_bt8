﻿using Banking.Models;
using Microsoft.EntityFrameworkCore;

namespace Banking.DataAccess
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options ) : base(options)
        {

        }
        public DbSet<Accounts> Accounts { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Transactions> Transactions { get; set; }
        public DbSet<logs> logs { get; set; }
        public DbSet<Employees> Employees { get; set; }
        public DbSet<Reports> Reports { get; set; }
    }
}
