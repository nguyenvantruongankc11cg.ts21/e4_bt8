﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Banking.Models
{
    public class Transactions
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Employees")]
        public int EmployeesId { get; set; }
        [ForeignKey("Customer")]
        public int CustomerId { get; set; }
        public string ? Name { get; set; }
        public Customer? Customer { get; set; }
        public Employees? Employees { get; set; }
    }
}
