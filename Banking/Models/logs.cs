﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Banking.Models
{
    public class logs
    {
        [Key]
        public int Id { get; set; }
       
        public DateTime LoginDate { get; set; }
        public DateTime LoginTime { get; set; } 

        [ForeignKey("Transactions")]
        public int TransactionsId { get; set; }
        public Transactions ? Transactions { get; set; }

    }
}
