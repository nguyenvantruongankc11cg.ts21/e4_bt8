﻿using System.ComponentModel.DataAnnotations;

namespace Banking.Models
{
    public class Customer
    {
        [Key]
        public int Id { get; set; }
        public string ? FirstName { get; set; }
        public string ? LastName { get; set; }
        public string ? ContactandAddress  { get; set; }
        public string ? UserName { get; set; }
        public string ? Password { get; set; }
    }
}
