﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Banking.Models
{
    public class Accounts
    {
        [Key]
        public int Id { get; set; }
        public string ? AccountName { get; set; }
        [ForeignKey("Customer")]
        public int CustomersId { get; set; }
        public Customer ?Customer { get; set; }
    }
}
