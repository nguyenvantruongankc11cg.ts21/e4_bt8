﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Banking.Models
{
    public class Reports
    {
        [Key]
        public int Id { get; set; }
        public string ? ReportName { get; set; }
        public DateTime ReportDate { get; set; }
        [ForeignKey("Accounts")]
        public int ? AccountId { get; set; }
        [ForeignKey("Transactions")]
        public int ? TransactiontId { get; set; }
        [ForeignKey("logs")]
        public int ? logsId { get; set; }
        public Transactions ? Transactions { get; set; }  
        public Accounts? Accounts { get; set; }
        public logs ? Logs { get; set; }

    }
}
