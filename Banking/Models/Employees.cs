﻿using System.ComponentModel.DataAnnotations;

namespace Banking.Models
{
    public class Employees
    {
        [Key]
        public int Id { get; set; }
        public string ? FirstName { get; set; }
        public string ? LastName { get; set; }
        public string? ContactandAddress { get; set; }
        public string? UserNameAndPassword { get; set; }
    }
}
